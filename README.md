# JSON Bubble Chat
**Sulthan Mahendra M / 4210181019**

This program will implement a JSON Text into a Unity Object

This code use a Newtonsoft Package that can be imported from https://github.com/jilleJr/Newtonsoft.Json-for-Unity


## Code Flow

### 1. Get json from the website
````
 private void Start()
    {
        WWW json = new WWW(jsonLink);
        StartCoroutine(JSONToText(json));
    }

    IEnumerator JSONToText(WWW json)
    {
        yield return json;
        SetText(json);
        
    }
````

### 2. Deserialize json into the userID class
````
private string GetName(string json)
    {
        List<JsonID> userID = JsonConvert.DeserializeObject<List<JsonID>>(json);
        return userID[id].name.ToString();
    }

    private string GetEmail(string json)
    {
        List<JsonID> userID = JsonConvert.DeserializeObject<List<JsonID>>(json);
        return userID[id].email.ToString();
    }

````
### 3. Return name / email of the userID class
````
[System.Serializable]
public class JsonID
{
    public string id;
    public string name;
    public string email;
}
````
### 4. Set the text with the name / email 
````
 private void SetText(WWW json){
        
        nameText.text = GetName(json.text);
        emailText.text = GetEmail(json.text);
    }
````

## GIF
![jsonGIF](JsonBubble.gif)
